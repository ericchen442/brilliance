﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Brilliance.Controllers
{
    [Route("api/arraycalc/[action]")]
    [ApiController]
    public class ArrayCalcController : ControllerBase
    {
        [HttpGet]
        [Produces("application/json")]
        public IActionResult reverse([FromQuery(Name = "productIds")]int[] productIds)
        {
            //reverse array        
            for (int i = 0; i < productIds.Length / 2; i++)
            {
                int temp;

                temp = productIds[i];

                productIds[i] = productIds[productIds.Length - i - 1];

                productIds[productIds.Length - i - 1] = temp;

            }

            return Ok(productIds);
        }


        [HttpGet]
        public IActionResult deletepart([FromQuery(Name = "productIds")]int[] productIds, [FromQuery(Name = "position")]int position)
        {
            //find position index in productsId array
            int indexofPosition = -1;

            for (int i = 0; i < productIds.Length; i++)
            {
                if (productIds[i] == position)
                {
                    indexofPosition = i;
                }

            }

            //remove position from array
            if (indexofPosition != -1)
            {
                var newArray = new int[productIds.Length - 1];

                int j = 0;

                for (int i = 0; i < productIds.Length; i++)
                {
                    //copy value from old array to new array except positin
                    if (i == indexofPosition)
                    {
                        continue;
                    }

                    newArray[j] = productIds[i];

                    j++;
                }

                return Ok(newArray);
            }


            //return original array if no position found
            return Ok(productIds);
        }
    }
}